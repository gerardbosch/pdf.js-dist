# pdf.js-dist w/ viewer (Mozilla copy)

Just a copy from release version of `pdf.js` that includes the viewer (from https://mozilla.github.io/pdf.js/)

Locked viewer version that can be included in other repositories as a Git submodule and that can be used to show PDFs on
devices that does not support displaying PDF objects embeded in HTML (e.g. mobile browsers in 2021).

This is intended to be used as a Git submodule:

```shell
git submodule add https://gitlab.com/gerardbosch/pdf.js-dist.git
git commit -am "Add pdf.js-dist submodule"
```

## Usage

You can use the viewer as the `src` in an iframe, e.g.

```html
<iframe id="pdf-js-viewer" src="pdf.js-dist/web/viewer.html?file=../../test.pdf" title="PDF viewer" style="width: 100%; height: 100%; overflow: hidden;"></iframe>
```

(The `file` location above must be relative to `viewer.html`).
